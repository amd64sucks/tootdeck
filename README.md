<img alt="Tootdeck logo" src="https://gitlab.com/tootdeck/tootdeck-frontend/-/raw/main/public/tootdeck.svg" width="150">

# TootDeck for Mastodon

![License: AGPL v3](https://img.shields.io/badge/License-AGPLv3-blue.svg)
![Pipeline Status](https://gitlab.com/tootdeck/tootdeck/badges/main/pipeline.svg?key_text=latest+commit&key_width=95)
![Pipeline Status](https://gitlab.com/tootdeck/tootdeck-backend/badges/main/pipeline.svg?key_text=latest+backend&key_width=95)
![Pipeline Status](https://gitlab.com/tootdeck/tootdeck-frontend/badges/main/pipeline.svg?key_text=latest+frontend&key_width=95)

# Features

TootDeck aims a to be an equivalent of TweetDeck for the fediverse, so it will implement all of its functionalities and even more over time.

-   Columns
    -   Federated timeline
    -   Local timeline
    -   Home timeline
    -   User (logged user statuses)
    -   Notifications
    -   Lists
    -   Favourites
    -   Bookmarks
    -   ... and more to come
-   Multi account with cross instance support
-   Follow/Favourite/Bookmark from any account in any column
-   Add/Remove account from a list from any account in any column
-   Quote status (see limitations) [Experimental]
-   Open status in the same column with thread indicators (it auto refreshes on new responses)
-   Mutual indicator at the bottom of the profile picture
-   List manager:
    -   Create
    -   Modify
    -   Delete
    -   Also works with any account in any column
-   Status interactions from remote instance (see limitations)
-   Only one websocket on the client side
-   Auto fetch missed statuses/notifications on websocket reconnection
-   User sessions with live notifcations on connection
-   Sync configuration (includes settings and columns layout) [Experimental]
-   ... and more

# Limitations

Authentication and data parsing is done by [Megalodon](https://github.com/h3poteto/megalodon),
only Mastodon and Pleroma/Akkoma are curently supported.

This will change in the future.

# How to run it

Create a `.env` and fill it up with your values

```bash
$ git clone https://gitlab.com/tootdeck/tootdeck.git --recursive
$ cd tootdeck
$ cp .env.template .env
```

Default values are available in `.env.default` (for dev purposes only)

## Production

You will need to edit `docker-compose.yml` to include your own network for reverse proxying.

**This app work exclusively with HTTPS**

```bash
$ docker compose up
```

## Dev

Dev mode is self contained, all you have to do is enter these 2 commands and go to https://localhost

```bash
$ pnpm install
$ docker compose -f docker-compose.dev.yml up
```

## Local testing

You can also run an local instance to test it, just enter this command and go to https://localhost

```bash
$ docker compose -f docker-compose.local.yml up
```

# License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses.
