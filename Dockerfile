#########################
FROM node:lts-bullseye-slim as common

ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

RUN apt-get update
RUN apt-get install -y procps nginx openssl wget
RUN rm -rf /var/cache/apt/archives /var/lib/apt/lists/*

# For running in local only
RUN openssl ecparam -name secp384r1 -genkey -out /etc/nginx/cert.key && \
	openssl req -x509 -out /etc/nginx/cert.crt -new -key /etc/nginx/cert.key -nodes -days 365 -subj '/C=US/L=Springfield/O=0/OU=localhost/CN=localhost'

WORKDIR /app

#########################
FROM common as build

COPY . /app
WORKDIR /app

ARG VITE_VERSION
RUN export VITE_VERSION=$VITE_VERSION

RUN npm i ffbinaries -g && \
	ffbinaries ffmpeg -v=4.4.1 -o=/usr/local/bin

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --frozen-lockfile
RUN pnpm run -r build
